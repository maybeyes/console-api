import React, {FC} from 'react';
import './Alert.css';

type AlertType = {
  value: string
}


const Alert: FC<AlertType> = (props) => {

  const {value} = props;

  return (
    <div className='Alert'>
      <span className='Alert__icon' />
      <span className='Alert__label'>Вход не вышел</span>
      <span className='Alert__error'>{value}</span>
    </div>
  );
};

export default Alert;