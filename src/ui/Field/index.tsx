import React, {FC} from 'react';
import './Field.css';
import {Field} from 'react-final-form';
import cn from 'classnames';

type FieldTextType = {
  keyField: string
  placeholder: string
  label: string
  className?: string
  validate?: boolean
  type?: string
}

export const FieldText: FC<FieldTextType> = (props) => {

  const {placeholder, keyField, label, className, validate = false, type = 'text'} = props;

  /*validation*/
  const required = (value: any) => (value ? undefined : 'Required');


  return (
    <Field name={keyField}
           validate={validate ? required : undefined}
           formatOnBlur={true}
    >
      {props => (
        <div className='field'>
          <label className={cn(className, {
            'field__label': true,
            'field__error__label': props.meta.error && validate && props.meta.touched,
          })}>{label}</label>
          <input className={cn(className, {
            'field__input': true,
            'field__error__input': props.meta.error && validate && props.meta.touched,
          })} {...props.input} placeholder={placeholder} type={type} />
        </div>
      )}
    </Field>
  );
};



