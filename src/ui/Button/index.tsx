import React, {FC} from 'react';
import './Button.css';
import cn from 'classnames';


type ButtonType = {
  disabled?: boolean
  variables?: string
  loading?: boolean
  handleClick?: () => void
} & React.HTMLAttributes<HTMLButtonElement>


const Button: FC<ButtonType> = (props) => {

  const {children, variables, loading, className, handleClick} = props;

  return (
    <button type='submit'
            onClick={handleClick}
            disabled={loading}
            className={cn(className, {
              'Button': true,
              [`Button--${variables}`]: true,
            })}
    >
      {children}
    </button>
  );
};

export default Button;