import React, {FC, useRef, useState} from 'react';
import useClipboard from 'react-use-clipboard';
import './Chip.css';

type ChipType = {
  children: string
  swiperRef: any
  id: string
  deleteItem: (id: string) => void
  runItem: (value: string) => void
  request: string
}

export const Chip: FC<ChipType> = (props) => {

  const {children, swiperRef, request, id, runItem, deleteItem} = props;

  /*hooks*/
  const ref = useRef<HTMLDivElement>(null);
  const [isOpened, setIsOpened] = useState<boolean>(false);
  const [value] = useState<string>(request);
  const [isCopied, setCopied] = useClipboard(request, {
    successDuration: 1000,
  });


  /*handlers*/
  const toggleHandler = () => {
    if (isOpened) {
      swiperRef.current.style.paddingBottom = '0';
      swiperRef.current.style.marginTop = '0';
    } else {
      swiperRef.current.style.paddingBottom = '160px';
      swiperRef.current.style.marginTop = '160px';
    }
    setIsOpened(!isOpened);
  };


  return (
    <div
      ref={ref}
      className='chip'
    >
      <span className='chip__indicator' />
      <span className='chip__title'>{isCopied ? 'скопировано' : children}</span>
      <img onClick={toggleHandler} src='/icons/dots.svg' alt='' className='chip__dropdown-handler' />
      {
        isOpened &&
        <div className='chip__dropdown'>
          <span className='chip__dropdown__run' onClick={() => runItem(value)}>Выполнить</span>
          <span className='chip__dropdown__copy' onClick={setCopied}>Скопировать</span>
          <span className='chip__dropdown__delete' onClick={() => deleteItem(id)}>Удалить</span>
        </div>
      }
    </div>
  );
};

