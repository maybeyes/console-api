import {call, put} from 'redux-saga/effects';
import {all, takeLatest} from 'typed-redux-saga';
import api from '../../helpers/sendsay';
import {authenticateFailure, authenticateSuccess,errorLogin} from '../actions/auth';
import {ActionTypes} from '../constants';

export function* authenticateCheckSaga() {
  try {
    yield api.sendsay.request({
      action: 'pong',
    });
  } catch (error) {
    if (error.id === 'error/auth/failed') {
      yield call(logoutSaga);
    }
  }
}

export function* authenticateSaga({payload}: any) {
  try{
    yield api.sendsay
      .login({
        login: payload.login,
        sublogin: payload.sublogin,
        password: payload.password,
      })
      .then(() => {
        document.cookie = `sendsay_session=${api.sendsay.session}`;
      })
      .catch((err: any) => {
        document.cookie = '';
        throw err
      });
    yield put(
      authenticateSuccess({
        sessionKey: api.sendsay.session,
        login: payload.login,
        sublogin: payload.sublogin,
      }),
    );
  }
  catch(error){
    yield put(errorLogin({error:JSON.stringify(error)}))
  }
}

export function* logoutSaga() {
  yield put(authenticateFailure());
  document.cookie = '';
}


export default function* root() {
  yield all([
    takeLatest(ActionTypes.AUTHENTICATE, authenticateSaga),
    takeLatest(ActionTypes.AUTHENTICATE_CHECK, authenticateCheckSaga),
    takeLatest(ActionTypes.LOGOUT, logoutSaga),
  ]);
}
