import {all, fork} from 'redux-saga/effects';

import login from '../sagas/auth';
import console from '../sagas/console';

export default function* root() {
  yield all([fork(login), fork(console)]);
}
