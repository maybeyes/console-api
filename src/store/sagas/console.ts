import {put} from 'redux-saga/effects';
import {all, takeLatest} from 'typed-redux-saga';
import {addHistoryRequest,response} from '../actions';
import api from '../../helpers/sendsay';
import {ActionTypes} from '../constants';

export function* sendAction({payload}: any) {
  try {
    yield api.sendsay.request(payload)
      .then(() => {

    })
      .catch((err: any) => {
        throw err;
      });
    yield put(addHistoryRequest({id:Date.now(),value:payload}))
  } catch (err) {
    yield put(response({value: err, type: 'error'}));
  }
}


export default function* root() {
  yield all([
    takeLatest(ActionTypes.SEND_REQUEST, sendAction),
  ]);
}
