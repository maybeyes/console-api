import {handleActions} from 'redux-actions';
import {ActionTypes} from '../constants';

type initialStateType = {
  loading: boolean
  sessionKey: string | null
  login: string | null
  sublogin: string | null
  error: string
}


export const initialState: initialStateType = {
  loading: false,
  sessionKey: null,
  login: null,
  sublogin: null,
  error: '',
};

export default {
  auth: handleActions(
    {
      [ActionTypes.AUTHENTICATE]: (state) => {
        return {
          ...state,
          loading: true,
        };
      },
      [ActionTypes.AUTHENTICATE_SUCCESS]: (state, {payload}) => {
        return {
          ...state,
          loading: false,
          sessionKey: payload.sessionKey,
          login: payload.login,
          sublogin: payload.sublogin,
        };
      },
      [ActionTypes.AUTHENTICATE_FAILURE]: (state) => {
        return {
          ...state,
          sessionKey: null,
          loading: false,
          login: null,
          sublogin: null,
          error: '',
        };
      },
      [ActionTypes.LOGOUT]: (state) => {
        return {
          ...initialState,
        };
      },
      [ActionTypes.ERROR_LOGIN]: (state, {payload}) => {
        return {
          ...state,
          error:payload.error,
          loading:false,
        };
      },
    },
    initialState,
  ),
};
