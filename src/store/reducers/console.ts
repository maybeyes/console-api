import {handleActions} from 'redux-actions';
import {ActionTypes} from '../constants';

type initialStateType = {
  historyActions: any[]
  response: any
}


export const initialState: initialStateType = {
  historyActions: [],
  response: '',
};

export default {
  console: handleActions(
    {
      [ActionTypes.SEND_REQUEST]: (state, {payload}) => {
        return {
          ...state,
        };
      },
      [ActionTypes.ADD_HISTORY_REQUEST]: (state, {payload}) => {
        return {
          ...state,
          historyActions: [...state.historyActions, payload],
        };
      },
      [ActionTypes.DELETE_HISTORY_REQUEST]: (state, {payload}) => {
        return {
          ...state,
          historyActions: state.historyActions.filter((item)=>item.id === payload),
        };
      },
      [ActionTypes.RESPONSE]: (state, {payload}) => {
        return {
          ...state,
          response: payload,
        };
      },
    },
    initialState,
  ),
};
