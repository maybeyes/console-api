import login from '../reducers/auth';
import console from '../reducers/console';

export default {
  ...login,
  ...console,
};
