import keyMirror from 'keymirror';

export const ActionTypes = keyMirror({
  AUTHENTICATE: undefined,
  AUTHENTICATE_CHECK: undefined,
  AUTHENTICATE_SUCCESS: undefined,
  AUTHENTICATE_FAILURE: undefined,
  LOGOUT: undefined,
  LOGOUT_SUCCESS: undefined,
  LOGOUT_FAILURE: undefined,
  ERROR_LOGIN: undefined,
  SEND_REQUEST: undefined,
  RESPONSE: undefined,
  ADD_HISTORY_REQUEST: undefined,
  DELETE_HISTORY_REQUEST: undefined,
});
