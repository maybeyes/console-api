import {createActions} from 'redux-actions';
import {ActionTypes} from '../constants';

export const {sendRequest,deleteHistoryRequest,addHistoryRequest,response} = createActions({
  [ActionTypes.SEND_REQUEST]: (payload) => payload,
  [ActionTypes.DELETE_HISTORY_REQUEST]: (payload) => payload,
  [ActionTypes.ADD_HISTORY_REQUEST]: (payload) => payload,
  [ActionTypes.RESPONSE]: (payload) => payload,
});
