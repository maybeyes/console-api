import React, {useRef, useState, MouseEvent, FC} from 'react';
import styled from 'styled-components';
import Button from '../../ui/Button';
import {sendRequest} from '../../store/actions';
import {useDispatch, useSelector} from 'react-redux';

const ConsoleWrapper = styled.div`
  padding:10px 15px;
  flex-grow: 1;
  display: flex;
  width: 100%;
  background:#FFFFFF;
`;

const Box = styled.div`
  flex-direction: column;
  box-sizing: border-box;
  flex: 1 1 auto;
  display:flex;
`;


const TextAreaWrapper = styled.div`
    width:100%;
    height:100%;  
`;


const CodeAreaWrapper = styled.div<any>`
    width:100%;
    height:100%;  
    border: ${props => props.type || '1px solid black'};
`;

const Area = styled.textarea`
    padding: 0;
    min-height:100%;
    min-width:100%;
    resize:none;
    :active, :hover, :focus {
    outline: 0;
    outline-offset: 0;
}
`;


const Draggable = styled.div`
  width: 20px;
  padding: 0;
  cursor: ew-resize;
  flex: 0 0 auto;
`;


const FooterWrapper = styled.div`
  width:100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background:#FFFFFF;
  padding:15px 15px;
`;


const LeftWrapper = styled.div`
  display:flex;
  justify-content:flex-start;
  align-items:center;
`;

const RightWrapper = styled.div`
  display:flex;
  justify-content:flex-end;
  align-items:center;
`;

const FormatIconStyle = styled.img`
  margin-right: 5px;
`;


const Console: FC = () => {


  /*hooks*/
  const [isDrag, setIsDrag] = useState<boolean>(false);
  const dispatch = useDispatch();
  const handlerRef = useRef<HTMLDivElement>(null);
  const boxOneRef = useRef<HTMLDivElement>(null);
  const boxTwoRef = useRef<HTMLDivElement>(null);
  const wrapperRef = useRef<HTMLDivElement>(null);
  const [valueArea, setValueArea] = useState<string>('');
  const response = useSelector((state: any) => state.console.response);

  /*drag events*/
  const handleActiveDrag = (e: MouseEvent) => {
    if (e.target === handlerRef.current) {
      setIsDrag(true);
    }
  };


  const handleMouseMove = (e: MouseEvent) => {
    if (!isDrag) {
      return false;
    }
    let containerOffsetLeft = wrapperRef.current!.offsetLeft;
    let pointerRelativeXpos = e.clientX - containerOffsetLeft;
    boxOneRef.current!.style.width = (pointerRelativeXpos - 8) + 'px';
    boxOneRef.current!.style.flexGrow = '0';
  };

  const handleUnActiveDrag = () => {
    setIsDrag(false);
  };


  /*handlers*/
  const handleChangeArea = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const value = e.target.value;
    setValueArea(value);
  };

  const sendAction = () => {
    dispatch(sendRequest(valueArea));
  };


  const handleFormatJson = () => {
    const parse = JSON.parse(valueArea);
    setValueArea(JSON.stringify(parse, undefined, 4));
  };


  return (
    <>
      <ConsoleWrapper ref={wrapperRef} onMouseMove={handleMouseMove} onMouseUp={handleUnActiveDrag}>
        <Box ref={boxOneRef}>
          <span>Запрос</span>
          <TextAreaWrapper>
            <Area value={valueArea} onChange={handleChangeArea} />
          </TextAreaWrapper>
        </Box>
        <Draggable ref={handlerRef} onMouseDown={handleActiveDrag} />
        <Box ref={boxTwoRef}>
          <span>ответ</span>
          <CodeAreaWrapper type={response.type === 'error' ? '1px solid red' : '1px solid black'}>
            <div>
              <pre>{JSON.stringify(response, null, 2) === `""` ? '' : JSON.stringify(response, null, 2)}</pre>
            </div>
          </CodeAreaWrapper>
        </Box>
      </ConsoleWrapper>
      <FooterWrapper>
        <LeftWrapper>
          <Button handleClick={sendAction}>Отправить</Button>
        </LeftWrapper>
        <a>@link-to-your-github</a>
        <RightWrapper>
          <FormatIconStyle src="/icons/format.svg" alt="" onClick={handleFormatJson} />
          <Button variables='outline'>Формотировать</Button>
        </RightWrapper>
      </FooterWrapper>
    </>
  );
};

export default Console;