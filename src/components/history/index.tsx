import React, {FC, useRef} from 'react';
import styled from 'styled-components';
import {Chip} from '../../ui/Chip';
import {SwiperSlide} from 'swiper/react';
import Swiper from 'react-id-swiper';
import './swiper.css';
import {useDispatch, useSelector} from 'react-redux';
import {deleteHistoryRequest, sendRequest} from '../../store/actions';


const MainWrapper = styled.div`
  width:100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background:#F6F6F6;
  padding:15px 15px;
  height: 60px;
`;


const CrossIconStyled = styled.img`
    margin-left:5px;
`;


const History: FC = () => {

  /*hooks*/
  const swiperRef = useRef(null);
  const historyActions = useSelector((state: any) => state.console.historyActions);
  const dispatch = useDispatch();

  const handleDelete = (id: string) => {
    dispatch(deleteHistoryRequest(id));
  };

  const handleRun = (value: string) => {
    dispatch(sendRequest(value));
  };

  return (
    <MainWrapper>
      <Swiper
        spaceBetween={15}
        slidesPerView={15}
        freeMode={true}
        scrollbar={{draggable: true}}
        ref={swiperRef}
      >
        {historyActions.map((item: any) => {
          return <SwiperSlide>
            <Chip id={item.id} request={item.value} swiperRef={swiperRef} deleteItem={handleDelete} runItem={handleRun}
                  key={item.id}>issue.send</Chip>
          </SwiperSlide>;
        })}
      </Swiper>
      <CrossIconStyled src="/icons/cross.svg" alt="" />
    </MainWrapper>
  );
};

export default History;
