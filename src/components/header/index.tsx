import React, {FC, useState} from 'react';
import styled from 'styled-components';
import {logout} from '../../store/actions';
import {useDispatch, useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom';


const MainWrapper = styled.div`
  height: 70px;
  width:100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background:#FFFFFF;
  padding:15px 15px;
`;

const LogoStyled = styled.img`
  margin-right:20px;
`;

const LabelStyled = styled.h1`
  font-family: 'Roboto', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  line-height: 30px;
`;


const LeftWrapper = styled.div`
  display:flex;
  justify-content:flex-start;
  align-items:center;
`;

const RightWrapper = styled.div`
  display:flex;
  justify-content:flex-end;
  align-items:center;
`;


const IconStyled = styled.img`
  cursor:pointer;
`;


const LogOutWrapperStyled = styled.div`
   display:flex;
   justify-content:space-between;
   margin-right:20px;
   cursor:pointer;
`;


const SubLoginWrapperStyled = styled.div`
   display:flex;
   justify-content:center;
   border-radius:5px;
   border: 1px solid grey;
   margin-right:20px;
   padding:5px 10px;
`;

const DotsIconStyled = styled.img`
  margin-right:5px;
`;

const SubLoginValueStyled = styled.span`
   margin-right:5px;
`;

const SubLoginTitleStyled = styled.span`
`;


type HeaderType = {
  consolePageRef: any
}

const Header: FC<HeaderType> = (props) => {

  const {consolePageRef} = props;

  /*hooks*/
  const dispatch = useDispatch();
  const history = useHistory();
  const [fullScreen, setFullScreen] = useState<boolean>(false);
  const sublogin = useSelector((state: any) => state.auth.sublogin);
  /*handlers*/
  const onLogOut = () => {
    dispatch(logout());
    history.push('/');
  };

  const fullScreenHandler = () => {
    console.log(consolePageRef.current);
    if (fullScreen) {
      consolePageRef.current.style.padding = '100px';
      setFullScreen(!fullScreen);
    } else {
      consolePageRef.current.style.padding = '0';
      setFullScreen(!fullScreen);
    }
  };


  return (
    <MainWrapper>
      <LeftWrapper>
        <LogoStyled src="/icons/logo.svg" alt="" />
        <LabelStyled>API-консолька</LabelStyled>
      </LeftWrapper>
      <RightWrapper>
        {sublogin && (
          <SubLoginWrapperStyled>
            <SubLoginValueStyled>some@email.com</SubLoginValueStyled>
            <DotsIconStyled src="/icons/dots.svg" alt="" />
            <SubLoginTitleStyled>sublogin</SubLoginTitleStyled>
          </SubLoginWrapperStyled>
        )}
        <LogOutWrapperStyled onClick={onLogOut}>
          <IconStyled src="/icons/log-out.svg" alt="" style={{marginRight: '20px'}} />
          <p>Выйти</p>
        </LogOutWrapperStyled>
        {fullScreen ?
          (<IconStyled onClick={fullScreenHandler} src="/icons/full-screen.svg" alt="" />)
          :
          (<IconStyled onClick={fullScreenHandler} src="/icons/full-screen.svg" alt="" />)
        }
      </RightWrapper>
    </MainWrapper>
  );
};

export default Header;