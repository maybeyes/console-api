import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Form} from 'react-final-form';
import {useHistory} from 'react-router-dom';
import styled from 'styled-components';
import {authenticate} from '../store/actions';
import {FieldText} from '../ui/Field';
import Alert from '../ui/Alert';

const MainWrapper = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const Container = styled.section`
  width: 520px;
  left: calc(50% - 520px / 2);
  top: 222px;
  background: #ffffff;
  display:flex;
  flex-direction:column;
  box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
  border-radius: 5px;
  padding: 40px 30px;
`;


const LogoStyled = styled.img`
  margin-bottom: 20px;
`;


const LabelStyled = styled.h1`
  font-family: 'Roboto', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 30px;
`;


const Button = styled.button`
    min-width: 100px;
    height: 40px;
    color: #FFF;
    font-size: 16px;
    border: none;
    outline: none;
    cursor: pointer;
    background: linear-gradient(180deg, #45A6FF 0%, #0055FB 100%), #C4C4C4;
    border-radius: 5px;
    font-family: 'Roboto', sans-serif;
`;

const LoginPage = () => {

  /*hooks*/
  const dispatch = useDispatch();
  const loading = useSelector((state: any) => state.auth.loading);
  const isLoggedIn = useSelector((state: any) => state.auth.sessionKey);
  const error = useSelector((state: any) => state.auth.error);
  const history = useHistory();

  /*effects*/
  useEffect(() => {
    console.log(isLoggedIn);
    if (isLoggedIn) {
      history.push('/console');
    }
  }, [isLoggedIn]);


  /*handlers*/
  const onSubmit = ({login, sublogin, password}: {login: string, sublogin: string, password: string}) => {
    console.log({login, sublogin, password});
    dispatch(authenticate({login, sublogin, password}));
  };

  return (
    <MainWrapper>
      <LogoStyled src="/icons/logo.svg" alt="" />
      <Container>
        <LabelStyled>API-консолька</LabelStyled>
        <Form
          onSubmit={onSubmit}
          initialValues={{stooge: 'larry', employed: false}}
          render={({handleSubmit}) => (
            <form onSubmit={handleSubmit}>
              {error && <Alert value={error} />}
              <FieldText keyField="login" validate={true} placeholder='login' label='Логин' />
              <FieldText keyField="sublogin" placeholder='sublogin' label='Сублогин' />
              <FieldText keyField="password" validate={true} placeholder='password' label='Пароль' type='password' />
              <Button type="submit">Войти</Button>
            </form>
          )}
        />
      </Container>
    </MainWrapper>
  );
};

export default LoginPage;
