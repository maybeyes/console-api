import React, {FC, useEffect, useRef} from 'react';
import {useSelector} from 'react-redux';
import {useHistory, withRouter} from 'react-router-dom';
import styled from 'styled-components';
import Header from '../components/header';
import Console from '../components/console';
import History from '../components/history';


const MainWrapper = styled.div`
  height: 100%;
  width:100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  padding:100px;
`;


const ConsolePage: FC = () => {

  /*hooks*/
  const history = useHistory();
  const isLoggedIn = useSelector((state: any) => state.auth.sessionKey);
  const consolePageRef = useRef<HTMLDivElement>(null);


  /*effects*/
  useEffect(() => {
    if (isLoggedIn) {
    } else {
      history.push('/');
    }
  }, [isLoggedIn]);

  return (
    <MainWrapper ref={consolePageRef}>
      <Header consolePageRef={consolePageRef} />
      <History />
      <Console />
    </MainWrapper>
  );
};

export default withRouter(ConsolePage);
